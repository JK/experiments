# Rendering of tutorial web pages

When updating tutorials/examples or adding new ones, you can commit the changes to the 
repository and see the rendered version 
[online](https://opencarp.org/documentation/examples) after the continuous 
integration pipeline has finished. 

# Adding new tutorials
Just add a new folder below `01_EP_single_cell` or `02_EP_tissue`. Follow the naming scheme and numbering. In your new folder, at least two files are required:
* `__init__.py` holding the metadata (title, description, cover image). Format see existing tutorials.
* `run.py` holding the body of the tutorial description as reStructuredText (RST)

You can include [basic RST commands](https://docutils.sourceforge.io/docs/user/rst/quickref.html) 
like references and links.
If you want to add images, put them in the `images` folder and prefix the 
filename with the tutorial number.

The existing tutorials are a good source of inspiration.

# Basic local rendering
You can also convert the reStructuredText  (RST) from the run.py files to basic 
HTML pages locally. To do so, `pandoc` and `pandoc-citeproc` are required.

As further preparatory steps, go to the root folder of this repository and
* clone a minimal version of the web page `git clone https://git.opencarp.org/openCARP/minimial-website.git`
* rename it `mv minimal-website opencarp.org`
* install the openCARP-CI Python package `pip install git+https://git.opencarp.org/openCARP/openCARP-CI.git`

Each time you updated the RST in one of the run.py files, run the pipeline
`run_docstring_pipeline --pipeline experiments --pipeline-source tutorials --pipeline-images images --pipeline-header .pipeline/header.html --pipeline-footer .pipeline/footer.html --pipeline-refs .pipeline/refs.yml --grav-path opencarp.org --log-level DEBUG --output-html`

The output is generated as plain HTML files in the respective subfolders of
opencarp.org, for example `experiments/opencarp.org/pages/05.documentation/01.examples/01_ep_single_cell/01_basic_bench/default.html`

