__title__ = 'EP heterogeneity'
__description__ = 'This example details how to assign different single cell dynamics to different parts of a simulated tissue slice using region-wise tagging'
__image__ = '/images/02_05C_Conductive_Heterogeneity_Fig2_Incr-gNa.png'