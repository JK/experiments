__title__ = 'Meshalyzer'
__description__ = 'meshalyzer is a 4D visualization tool for openCARP allowing interactions to investigate data'
__image__ = '/images/vis_meshalyzer_favicon.png'
__GUIinclude__ = False
