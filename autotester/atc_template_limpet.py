#!/usr/bin/env python
#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#


import os
import argparse
import re
from string import Template
from carputils import settings
import bench

parser = argparse.ArgumentParser()

parser.add_argument('--keep-output',
                    action='store_true',
                    help='Do not delete simulation outputs')

args = parser.parse_args()


atc_template_head_IM = '''<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<atc>'''

atc_template_head_PLUGIN = '''<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<atc>'''
      
atc_template_body_IM ='''
  <test name="$imp" group="LIMPET-IM">
    <call>
      <command>python3 $dirname/regression/devtests/LIMPET/ionic_model/LIMPET_IM.py --model=$imp</command>
    </call>
    <validations>
      <validation>
        <variables>
          <variable name="reference_folder">"$refdirname/devtests/LIMPET.ionic_model.run</variable>
          <variable name="tol">1e-6</variable>
        </variables>
        <description>Checking maximum error between the test result and the reference for one Ionic Model</description>
        <call>
          <command>python3 $dirname/autotester/max_error.py --result  $imp/$imp.Vm.bin --reference $reference_folder$/$imp/$imp.Vm.bin"</command>
        </call>
      </validation>'''

atc_template_body_PLUGIN_E ='''
  <test name="$imp" group="LIMPET-PL-E">
    <call>
      <command>python3 $dirname/regression/devtests/LIMPET/plugin/LIMPET_PL.py --plugin=$imp</command>
    </call>
    <validations>
      <validation>
        <variables>
          <variable name="reference_folder">"$refdirname/devtests/LIMPET.plugin.run</variable>
          <variable name="tol">1e-6</variable>
        </variables>
        <description>Checking maximum error between the test result and the reference for one Ionic Model</description>
        <call>
          <command>python3 $dirname/autotester/max_error.py --result  $imp/$imp.Iion.bin --reference $reference_folder$/$imp/$imp.Iion.bin"</command>
        </call>
      </validation>'''

atc_template_body_PLUGIN_T ='''
  <test name="$imp" group="LIMPET-PL-T">
    <call>
      <command>python3 $dirname/regression/devtests/LIMPET/em_coupling/run.py --plugin=$imp</command>
    </call>
    <validations>
      <validation>
        <variables>
          <variable name="reference_folder">"$refdirname/devtests/LIMPET.plugin.run</variable>
          <variable name="tol">1e-6</variable>
        </variables>
        <description>Checking maximum error between the test result and the reference for one Ionic Model</description>
        <call>
          <command>python3 $dirname/autotester/max_error.py --result  $imp/$imp.Tension.bin --reference $reference_folder$/$imp/$imp.Tension.bin"</command>
        </call>
      </validation>'''

if args.keep_output:
    atc_template_test_footer = '''
    </validations>
  </test>'''
else:
    # Delete results generated during test
    atc_template_test_footer = '''
      <validation>
        <description>Remove test folder</description>
        <call>
          <command>rm -r $imp</command>
        </call>
      </validation>
    </validations>
  </test>'''


atc_template_footer ='''
</atc>'''


def run():
  
  imps = bench.imps()
  imps.remove("Campos") #Excluded
  atc = Template(atc_template_body_IM)
  atc_test_footer = Template(atc_template_test_footer)
  atc_head_IM = Template(atc_template_head_IM)
  dirname, _ = os.path.split(os.path.abspath(__file__))
  refdirname = settings.config.REGRESSION_REF
  parentpath = os.path.abspath(os.path.join(dirname, os.pardir))
  file = open(parentpath+'/regression/devtests/LIMPET/ionic_model/ionic_model.atc', 'w')
  file.write(atc_head_IM.safe_substitute(dirname=parentpath,refdirname=refdirname))
  for IMP in imps:
    file.write(atc.safe_substitute(imp=IMP,dirname=parentpath,refdirname=refdirname))
    file.write(atc_test_footer.safe_substitute(imp=IMP))
  file.write(atc_template_footer)
  file.close()
  
  imps = []; imp_e = []; imp_t = []
  imps = bench.plugins()
  for imp in imps:
    if re.search(r'Stress*',imp) is not None:
      imp_t.append(imp)
    else:
      imp_e.append(imp)

  # Electrophysiology plugins
  atc = Template(atc_template_body_PLUGIN_E)
  atc_head_PLUGIN = Template(atc_template_head_PLUGIN)
  dirname, _ = os.path.split(os.path.abspath(__file__))
  parentpath = os.path.abspath(os.path.join(dirname, os.pardir))
  file = open(parentpath+'/regression/devtests/LIMPET/plugin/plugins_e.atc', 'w')
  file.write(atc_head_PLUGIN.safe_substitute(dirname=parentpath,refdirname=refdirname))
  for IMP in imp_e:
    file.write(atc.safe_substitute(imp=IMP,dirname=parentpath,refdirname=refdirname))
    file.write(atc_test_footer.safe_substitute(imp=IMP))
  file.write(atc_template_footer)
  file.close()

  # Tension plugins
  atc = Template(atc_template_body_PLUGIN_T)
  atc_head_PLUGIN = Template(atc_template_head_PLUGIN)
  dirname, _ = os.path.split(os.path.abspath(__file__))
  parentpath = os.path.abspath(os.path.join(dirname, os.pardir))
  file = open(parentpath+'/regression/devtests/LIMPET/em_coupling/plugins_t.atc', 'w')
  file.write(atc_head_PLUGIN.safe_substitute(dirname=parentpath,refdirname=refdirname))
  for IMP in imp_t:
    file.write(atc.safe_substitute(imp=IMP,dirname=parentpath,refdirname=refdirname))
    file.write(atc_test_footer.safe_substitute(imp=IMP))
  file.write(atc_template_footer)
  file.close()

if __name__ == '__main__':
    run()
