#!/usr/bin/env python

"""
This is an example used to test if the Land Problem 1 (bending beam with anisotropic Guccione material) is satisfied.

Problem Setup
=============

Geometry of the bending beam in mm:

.. math::

    0.0  \leq x \leq 10.0

    0.0 \leq y \leq 1.0

    0.0 \leq z \leq 1.0

Pressure of 4 Pa is applied on the bottom surface.

Usage
=====

Everything in this example is preset.

.. code-block:: bash

    ./run.py
"""
import os

EXAMPLE_DESCRIPTIVE_NAME = 'Land Benchmark Problem 1'
EXAMPLE_AUTHOR = 'Jonathan Krauß <jonathan.krauss@kit.edu>'
EXAMPLE_DIR = os.path.dirname(__file__)

from datetime import date

from carputils import settings
from carputils import tools
from carputils import testing

def parser():
    parser = tools.standard_parser()
    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    return '{}_Land_prob1_np{}'.format(today.isoformat(), args.np)

@tools.carpexample(parser, jobID)
def run(args, job):


    # Get basic command line, including solver options
    cmd = tools.carp_cmd(os.path.join(EXAMPLE_DIR, 'Land_prob1.par'))

    cmd += ['-simID',    job.ID]


    # Run simulation
    job.carp(cmd)


# Define some tests
desc = ('Bend small beam and compare '
       ':math:`x` against reference.')
test_Lp1 = testing.Test('default', run, description=desc,
                            tags=[testing.tag.FAST, testing.tag.SERIAL])
test_Lp1.add_filecmp_check('Land_prob1.dynpt', testing.max_error, 0.001)
#
#
__tests__ = [test_Lp1]

if __name__ == '__main__':
    run()
