#!/usr/bin/env python

"""
This is an example used to test if the Land Problem 2 (idealized ventricle with homogenous pressure boundary condition) is satisfied.

Problem Setup
=============

Geometry of the ellipsoid:

.. math::
    x  =  r_\mathrm{s} \mathrm{sin} u \mathrm{cos} v

    y  =  r_\mathrm{s} \mathrm{sin} u \,athrm{sin} v 

    z  =  r_\mathrm{l} \mathrm{cos} u

    \text{endocardial surface}:  
    r_\mathrm{s} = 10\,\mathrm{mm}
    r_\mathrm{l} = 17\,\mathrm{mm}
    u \in [- \pi, -\mathrm{arccos} \frac{5}{17}]
    v \in [- \pi, \pi]

    \text{epicardial surface}:
    r_\mathrm{s} = 10\,\mathrm{mm}
    r_\mathrm{l} = 20\,\mathrm{mm}
    u \in [- \pi, -\mathrm{arccos} \frac{5}{20}]
    v \in [- \pi, \pi]

    \text{base plane}:
    z = 5\,\mathrm{mm}

Pressure of 10 kPa is applied on the endocardial surface.

Usage
=====

Everything in this example is preset.

.. code-block:: bash

    ./run.py
"""
import os

EXAMPLE_DESCRIPTIVE_NAME = 'Land Benchmark Problem 2'
EXAMPLE_AUTHOR = 'Jonathan Krauß <jonathan.krauss@kit.edu>'
EXAMPLE_DIR = os.path.dirname(__file__)

from datetime import date

from carputils import settings
from carputils import tools
from carputils import testing

def parser():
    parser = tools.standard_parser()
    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    return '{}_Land_prob2_np{}'.format(today.isoformat(), args.np)

@tools.carpexample(parser, jobID)
def run(args, job):


    # Get basic command line, including solver options
    cmd = tools.carp_cmd(os.path.join(EXAMPLE_DIR, 'Land_prob2.par'))

    cmd += ['-simID',    job.ID]


    # Run simulation
    job.carp(cmd)


# Define some tests
#desc = ('Generate short activation sequence on small cuboid and compare '
 #       ':math:`V_m` against reference.')
#test_default = testing.Test('default', run, description=desc,
 #                           tags=[testing.tag.FAST, testing.tag.SERIAL])
#test_default.add_filecmp_check('vm.igb.gz', testing.max_error, 0.001)


#__tests__ = [test_default]

if __name__ == '__main__':
    run()
