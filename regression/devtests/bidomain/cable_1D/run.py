#!/usr/bin/env python

"""
One dimensional cable EP simulation.
"""
import os

EXAMPLE_DESCRIPTIVE_NAME = '1D Cable Bidomain'
EXAMPLE_AUTHOR = 'Unassigned'
EXAMPLE_DIR = os.path.dirname(__file__)

from datetime import date

from carputils import settings
from carputils import tools
from carputils import mesh
from carputils import testing


def parser():
    parser = tools.standard_parser()
    parser.add_argument('--spacing',
                        type=float, default=25.,
                        help='Specify the cable node spacing in um (default '
                            +'25 um)')
    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    return '{}_cable_{}um_np{}'.format(today.isoformat(),
                                       args.spacing, args.np)

@tools.carpexample(parser, jobID)
def run(args, job):

    # Generate mesh
    # Units are mm
    # 1D cable with requested spacing
    geom = mesh.Cable(0, 50, args.spacing/1000.)

    meshdir  = 'mesh'
    meshname = '{}/cable'.format(meshdir)

    if not os.path.exists(meshdir):
        os.mkdir(meshdir)
    
    # Generate mesh files
    geom.generate_carp(meshname)

    # Get basic command line, including solver options
    cmd = tools.carp_cmd(os.path.join(EXAMPLE_DIR, 'cable_1D.par'))

    cmd += ['-simID',    job.ID,
            '-meshname', meshname]

    # Run simulation 
    job.carp(cmd)

    # Do visualization
    if args.visualize and not settings.platform.BATCH:

        # Prepare file paths
        geom = os.path.join(job.ID, os.path.basename(meshname)+'_i')
        data = os.path.join(job.ID, 'vm.igb.gz')
        view = os.path.join(EXAMPLE_DIR, 'cable_1D.mshz')
        
        # Call meshalyzer
        job.meshalyzer(geom, data, view)

if __name__ == '__main__':
    run()    
